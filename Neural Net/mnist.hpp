//
//  mnist.hpp
//  Neural Net
//
//  Created by John Lakness on 11/3/16.
//  Copyright © 2016 John Lakness. All rights reserved.
//

#ifndef mnist_hpp
#define mnist_hpp

#include <stdio.h>
#include <vector>

void ReadMNIST(const char* fname, std::vector<std::vector<double>> &arr);
void ReadLabels(const char* fname, std::vector<unsigned char> &arr);

#endif /* mnist_hpp */