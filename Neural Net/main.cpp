//
//  main.cpp
//  Neural Net
//
//  Created by John Lakness on 11/3/16.
//  Copyright © 2016 John Lakness. All rights reserved.
//

#include <iostream>
#include <vector>
#include <fstream>
#include <math.h>
#include <array>
#include <random>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/iterator/zip_iterator.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>
#include <boost/range/combine.hpp>
#include <boost/range/adaptor/reversed.hpp>
#include "mnist.hpp"

using namespace std;
using namespace boost::numeric;
using namespace boost::adaptors;

template <typename T> const T fnF(const T x){
    return 1.0/(1+exp(-1.0*x));
}
template <typename T> const T fnD(const T f){
    return f*(1-f);
}


int main()
{
    vector<vector<double>> X;
    ReadMNIST("/Users/johnlakness/projects/Neural Net/data/mnist/train-images-idx3-ubyte",X);
    vector<unsigned char> Y;
    ReadLabels("/Users/johnlakness/projects/Neural Net/data/mnist/train-labels-idx1-ubyte",Y);
    auto XY = boost::combine(X,Y);
    
    for(double xx:X.front())
        cout << xx << ' ';
    
    const int m_in = X.front().size(); //input width
    const int n_out = 10;
    vector<int> widths {m_in,100,100,n_out}; // layer widths
    
    // random number generator for parameter initialization
    default_random_engine generator;
    uniform_real_distribution<float> distribution(-1.0,1.0);
    
    //create parameter storage
    vector<ublas::vector<float>> V;  //neuron values
    //V.push_back(new ublas::vector<float>(m_in)) // inputs get loaded into first layer
    vector<ublas::matrix<float>> A; //transformation matrices
    vector<ublas::vector<float>> D; //pre-synaptic derivatives
    size_t m = m_in;
    for(int n:widths){
        // create neuron value vector
        auto v = new ublas::vector<float>(m);
        V.push_back(*v);
        // create/init transformation matrix
        auto a = new ublas::matrix<float>(m,n);
        for(auto i=a->begin1();i!=a->end1();++i)
            for(auto j=i.begin();j!=i.end();++j)
                *j = distribution(generator);
        A.push_back(*a); // possibly better to use std::make_shared
        // create pre-synaptic derivative vector
        auto d = new ublas::vector<float>(n);
        D.push_back(*d);
        // output width becomes input width for next layer
        m=n;
    }
    auto VAD = boost::combine(V,A,D); //zip iterator
    ublas::vector<float> v;
    ublas::matrix<float> a;
    ublas::vector<float> d;
    
    const int epochs(10);
    for(int epoch = 0;epoch<epochs;epoch++){
        for(const auto xy:XY){
            // assign variables for convenience
            vector<double> x;
            unsigned char y;
            boost::tie(x,y) = xy;
            // FPROP: apply matrix transformations u=F(uM)
            ublas::vector<float> u(x.size());
            copy(x.begin(),x.end(),x.begin());
            for(const auto & vad:VAD){ // F(u.a)->v
                boost::tie(v,a,d) = vad;
                v=u;
                //u.resize(a.size2());
                auto w = ublas::prod(v,a);
                u.resize(w.size());
                u=w;
                for_each(u.begin(),u.end(),fnF<float>);
            }
            // BPROP: reverse matrix transformations
            ublas::vector<float> t(u.size(),0.0);
            t[y] = 1.0;
            ublas::vector<float> dp = u-t;
            //ublas::vector<float> dp = dt;
            for(auto vad:reverse(VAD)){
                boost::tie(v,a,d) = vad;
                d = dp; // result from prior iteration
                auto dd = ublas::prod(a,d);
                dp.resize(v.size());
                transform(v.begin(), v.end(), dp.begin(), fnD<float>);
                transform(dp.begin(), dp.end(), dd.begin(), dp.begin(), multiplies<float>());
                // form gradient matrix
                ublas::matrix<float> da = ublas::outer_prod(dp, d);
                a = a - 0.01*da;
            }
            
        }
    }
    
    
    return 0;
}

/*
int main(int argc, const char * argv[]) {
    // insert code here...
    std::cout << "Hello, World!\n";
    return 0;
    
}
*/